import os
import httpx
import unittest

class TestAppetizer(unittest.TestCase):
    def test_appetizer_api(self):
        API_HOST = os.getenv('API_HOST')
        url = f'http://{API_HOST}'
        r = httpx.get(url)
        self.assertEqual(r.status_code, 200)
    
    def test_appetizer_get_item(self):
        API_HOST = os.getenv('API_HOST')
        item_id = 46
        q = "Test API"
        url = f'http://{API_HOST}/items/{item_id}'
        item_ans = {"item_id": item_id, "q": q}
        r = httpx.get(url, params={"q": q})
        item = r.json()
        self.assertEqual(item, item_ans)